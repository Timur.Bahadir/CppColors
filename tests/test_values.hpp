#include <cppc/cppc.hpp>

// Expected values are calculated using: http://colorizer.org/
constexpr cppc::rgba_values Expected_rgba{0.4431, 0.8667, 0.8902, 1.0};
constexpr cppc::rgba255_values Expected_rgba255{113, 221, 227, 255};
constexpr uint32_t Expected_rgbHex{0x71dde3};
constexpr uint32_t Expected_rgbaHex{0x71dde3ff};
constexpr cppc::hsva_values Expected_hsva{183.16, 0.5022, 0.8902, 1.0};
constexpr cppc::hsla_values Expected_hsla{183.16, 0.6706, 0.6667, 1.0};
constexpr cppc::cmyka_values Expected_cmyka{0.5022, 0.0264, 0.0, 0.1098, 1.0};

constexpr float Epsilon{0.01};