#include <catch.hpp>

#include <cppc/models.hpp>

#include "test_values.hpp"

#include <cstdint>

TEST_CASE("rgb cast operators", "[rgb]") {
  auto const rgb = cppc::rgba_values{Expected_rgba};

  auto const hsv = static_cast<cppc::hsva_values>(rgb);
  REQUIRE(hsv.h == Approx(Expected_hsva.h).epsilon(Epsilon));
  REQUIRE(hsv.s == Approx(Expected_hsva.s).epsilon(Epsilon));
  REQUIRE(hsv.v == Approx(Expected_hsva.v).epsilon(Epsilon));
  REQUIRE(hsv.a == Approx(Expected_hsva.a).epsilon(Epsilon));

  auto const hsl = static_cast<cppc::hsla_values>(rgb);
  REQUIRE(hsl.h == Approx(Expected_hsla.h).epsilon(Epsilon));
  REQUIRE(hsl.s == Approx(Expected_hsla.s).epsilon(Epsilon));
  REQUIRE(hsl.l == Approx(Expected_hsla.l).epsilon(Epsilon));
  REQUIRE(hsl.a == Approx(Expected_hsla.a).epsilon(Epsilon));

  auto const cmyk = static_cast<cppc::cmyka_values>(rgb);
  REQUIRE(cmyk.c == Approx(Expected_cmyka.c).epsilon(Epsilon));
  REQUIRE(cmyk.m == Approx(Expected_cmyka.m).epsilon(Epsilon));
  REQUIRE(cmyk.y == Approx(Expected_cmyka.y).epsilon(Epsilon));
  REQUIRE(cmyk.k == Approx(Expected_cmyka.k).epsilon(Epsilon));
  REQUIRE(cmyk.a == Approx(Expected_cmyka.a).epsilon(Epsilon));
}

TEST_CASE("hsv cast operators", "[hsv]") {
  auto const hsv = cppc::hsva_values{Expected_hsva};

  auto const rgb = static_cast<cppc::rgba_values>(hsv);
  REQUIRE(rgb.r == Approx(Expected_rgba.r).epsilon(Epsilon));
  REQUIRE(rgb.g == Approx(Expected_rgba.g).epsilon(Epsilon));
  REQUIRE(rgb.b == Approx(Expected_rgba.b).epsilon(Epsilon));
  REQUIRE(rgb.a == Approx(Expected_rgba.a).epsilon(Epsilon));

  auto const hsl = static_cast<cppc::hsla_values>(hsv);
  REQUIRE(hsl.h == Approx(Expected_hsla.h).epsilon(Epsilon));
  REQUIRE(hsl.s == Approx(Expected_hsla.s).epsilon(Epsilon));
  REQUIRE(hsl.l == Approx(Expected_hsla.l).epsilon(Epsilon));
  REQUIRE(hsl.a == Approx(Expected_hsla.a).epsilon(Epsilon));

  auto const cmyk = static_cast<cppc::cmyka_values>(hsv);
  REQUIRE(cmyk.c == Approx(Expected_cmyka.c).epsilon(Epsilon));
  REQUIRE(cmyk.m == Approx(Expected_cmyka.m).epsilon(Epsilon));
  REQUIRE(cmyk.y == Approx(Expected_cmyka.y).epsilon(Epsilon));
  REQUIRE(cmyk.k == Approx(Expected_cmyka.k).epsilon(Epsilon));
  REQUIRE(cmyk.a == Approx(Expected_cmyka.a).epsilon(Epsilon));
}

TEST_CASE("hsl cast operators", "[hsl]") {
  auto const hsl = cppc::hsla_values{Expected_hsla};

  auto const rgb = static_cast<cppc::rgba_values>(hsl);
  REQUIRE(rgb.r == Approx(Expected_rgba.r).epsilon(Epsilon));
  REQUIRE(rgb.g == Approx(Expected_rgba.g).epsilon(Epsilon));
  REQUIRE(rgb.b == Approx(Expected_rgba.b).epsilon(Epsilon));
  REQUIRE(rgb.a == Approx(Expected_rgba.a).epsilon(Epsilon));

  auto const hsv = static_cast<cppc::hsva_values>(hsl);
  REQUIRE(hsv.h == Approx(Expected_hsva.h).epsilon(Epsilon));
  REQUIRE(hsv.s == Approx(Expected_hsva.s).epsilon(Epsilon));
  REQUIRE(hsv.v == Approx(Expected_hsva.v).epsilon(Epsilon));
  REQUIRE(hsv.a == Approx(Expected_hsva.a).epsilon(Epsilon));

  auto const cmyk = static_cast<cppc::cmyka_values>(hsl);
  REQUIRE(cmyk.c == Approx(Expected_cmyka.c).epsilon(Epsilon));
  REQUIRE(cmyk.m == Approx(Expected_cmyka.m).epsilon(Epsilon));
  REQUIRE(cmyk.y == Approx(Expected_cmyka.y).epsilon(Epsilon));
  REQUIRE(cmyk.k == Approx(Expected_cmyka.k).epsilon(Epsilon));
  REQUIRE(cmyk.a == Approx(Expected_cmyka.a).epsilon(Epsilon));
}

TEST_CASE("cmyk cast operators", "[cmyk]") {
  auto const cmyk = cppc::cmyka_values{Expected_cmyka};

  auto const rgb = static_cast<cppc::rgba_values>(cmyk);
  REQUIRE(rgb.r == Approx(Expected_rgba.r).epsilon(Epsilon));
  REQUIRE(rgb.g == Approx(Expected_rgba.g).epsilon(Epsilon));
  REQUIRE(rgb.b == Approx(Expected_rgba.b).epsilon(Epsilon));
  REQUIRE(rgb.a == Approx(Expected_rgba.a).epsilon(Epsilon));

  auto const hsv = static_cast<cppc::hsva_values>(cmyk);
  REQUIRE(hsv.h == Approx(Expected_hsva.h).epsilon(Epsilon));
  REQUIRE(hsv.s == Approx(Expected_hsva.s).epsilon(Epsilon));
  REQUIRE(hsv.v == Approx(Expected_hsva.v).epsilon(Epsilon));
  REQUIRE(hsv.a == Approx(Expected_hsva.a).epsilon(Epsilon));

  auto const hsl = static_cast<cppc::hsla_values>(cmyk);
  REQUIRE(hsl.h == Approx(Expected_hsla.h).epsilon(Epsilon));
  REQUIRE(hsl.s == Approx(Expected_hsla.s).epsilon(Epsilon));
  REQUIRE(hsl.l == Approx(Expected_hsla.l).epsilon(Epsilon));
  REQUIRE(hsl.a == Approx(Expected_hsla.a).epsilon(Epsilon));
}
