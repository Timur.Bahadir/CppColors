#include <catch.hpp>

#include <cppc/cppc.hpp>

#include "test_values.hpp"

#include <cstdint>

#define CPPC_RGBA_TEST_SECTION()                                               \
  SECTION("to rgba") {                                                         \
    auto const rgb = color.rgba();                                             \
                                                                               \
    REQUIRE(rgb.r == Approx(Expected_rgba.r).epsilon(Epsilon));                \
    REQUIRE(rgb.g == Approx(Expected_rgba.g).epsilon(Epsilon));                \
    REQUIRE(rgb.b == Approx(Expected_rgba.b).epsilon(Epsilon));                \
    REQUIRE(rgb.a == Approx(Expected_rgba.a).epsilon(Epsilon));                \
  }

#define CPPC_RGBA255_TEST_SECTION()                                            \
  SECTION("to rgba255") {                                                      \
    auto const rgb255 = color.rgba255();                                       \
                                                                               \
    REQUIRE(+rgb255.r == +Expected_rgba255.r);                                 \
    REQUIRE(+rgb255.g == +Expected_rgba255.g);                                 \
    REQUIRE(+rgb255.b == +Expected_rgba255.b);                                 \
    REQUIRE(+rgb255.a == +Expected_rgba255.a);                                 \
  }

#define CPPC_RGBHEX_TEST_SECTION()                                             \
  SECTION("to rgbHex") {                                                       \
    auto rgbHex = color.rgbHex();                                              \
                                                                               \
    REQUIRE(rgbHex == Expected_rgbHex);                                        \
  }

#define CPPC_RGBAHEX_TEST_SECTION()                                            \
  SECTION("to rgbaHex") {                                                      \
    auto rgbHex = color.rgbaHex();                                             \
                                                                               \
    REQUIRE(rgbHex == Expected_rgbaHex);                                       \
  }

#define CPPC_HSVA_TEST_SECTION()                                               \
  SECTION("to hsv") {                                                          \
    auto hsv = color.hsva();                                                   \
                                                                               \
    REQUIRE(hsv.h == Approx(Expected_hsva.h).epsilon(Epsilon));                \
    REQUIRE(hsv.s == Approx(Expected_hsva.s).epsilon(Epsilon));                \
    REQUIRE(hsv.v == Approx(Expected_hsva.v).epsilon(Epsilon));                \
    REQUIRE(hsv.a == Approx(Expected_hsva.a).epsilon(Epsilon));                \
  }

#define CPPC_HSLA_TEST_SECTION()                                               \
  SECTION("to hsl") {                                                          \
    auto hsl = color.hsla();                                                   \
                                                                               \
    REQUIRE(hsl.h == Approx(Expected_hsla.h).epsilon(Epsilon));                \
    REQUIRE(hsl.s == Approx(Expected_hsla.s).epsilon(Epsilon));                \
    REQUIRE(hsl.l == Approx(Expected_hsla.l).epsilon(Epsilon));                \
    REQUIRE(hsl.a == Approx(Expected_hsla.a).epsilon(Epsilon));                \
  }

#define CPPC_CMYKA_TEST_SECTION()                                              \
  SECTION("to cmyka") {                                                        \
    auto cmyk = color.cmyka();                                                 \
                                                                               \
    REQUIRE(cmyk.c == Approx(Expected_cmyka.c).epsilon(Epsilon));              \
    REQUIRE(cmyk.m == Approx(Expected_cmyka.m).epsilon(Epsilon));              \
    REQUIRE(cmyk.y == Approx(Expected_cmyka.y).epsilon(Epsilon));              \
    REQUIRE(cmyk.k == Approx(Expected_cmyka.k).epsilon(Epsilon));              \
    REQUIRE(cmyk.a == Approx(Expected_cmyka.a).epsilon(Epsilon));              \
  }

/*
 ######   #####  ######
 #     # #     # #     #
 #     # #       #     #
 ######  #  #### ######
 #   #   #     # #     #
 #    #  #     # #     #
 #     #  #####  ######
*/
TEST_CASE("rgba conversions", "[rgb]") {
  auto const color =
      cppc::color::from_rgb(Expected_rgba.r, Expected_rgba.g, Expected_rgba.b);

  CPPC_RGBA255_TEST_SECTION()
  CPPC_RGBHEX_TEST_SECTION()
  CPPC_RGBAHEX_TEST_SECTION()
  CPPC_HSVA_TEST_SECTION()
  CPPC_HSLA_TEST_SECTION()
  CPPC_CMYKA_TEST_SECTION()
}

/*
 ######   #####  ######   #####  ####### #######
 #     # #     # #     # #     # #       #
 #     # #       #     #       # #       #
 ######  #  #### ######   #####  ######  ######
 #   #   #     # #     # #             #       #
 #    #  #     # #     # #       #     # #     #
 #     #  #####  ######  #######  #####   #####
*/
TEST_CASE("rgba255 conversions", "[rgb]") {
  auto const color = cppc::color::from_rgb255(
      Expected_rgba255.r, Expected_rgba255.g, Expected_rgba255.b);

  CPPC_RGBA_TEST_SECTION()
  CPPC_RGBHEX_TEST_SECTION()
  CPPC_RGBAHEX_TEST_SECTION()
  CPPC_HSVA_TEST_SECTION()
  CPPC_HSLA_TEST_SECTION()
  CPPC_CMYKA_TEST_SECTION()
}

/*
 ######   #####  ######  #     # ####### #     #
 #     # #     # #     # #     # #        #   #
 #     # #       #     # #     # #         # #
 ######  #  #### ######  ####### #####      #
 #   #   #     # #     # #     # #         # #
 #    #  #     # #     # #     # #        #   #
 #     #  #####  ######  #     # ####### #     #
*/
TEST_CASE("rgbHex conversions", "[rgb]") {
  auto const color = cppc::color::from_rgbHex(Expected_rgbHex);
  auto const rgbHex = color.rgbHex();

  CPPC_RGBA_TEST_SECTION()
  CPPC_RGBA255_TEST_SECTION()
  CPPC_RGBAHEX_TEST_SECTION()
  CPPC_HSVA_TEST_SECTION()
  CPPC_HSLA_TEST_SECTION()
  CPPC_CMYKA_TEST_SECTION()
}

/*
 #     #  #####  #     #
 #     # #     # #     #
 #     # #       #     #
 #######  #####  #     #
 #     #       #  #   #
 #     # #     #   # #
 #     #  #####     #
*/
TEST_CASE("hsv conversions", "[hsv]") {
  auto const color =
      cppc::color::from_hsv(Expected_hsva.h, Expected_hsva.s, Expected_hsva.v);

  CPPC_RGBA_TEST_SECTION()
  CPPC_RGBA255_TEST_SECTION()
  CPPC_RGBHEX_TEST_SECTION()
  CPPC_RGBAHEX_TEST_SECTION()
  CPPC_HSLA_TEST_SECTION()
  CPPC_CMYKA_TEST_SECTION()
}

/*
 #     #  #####  #
 #     # #     # #
 #     # #       #
 #######  #####  #
 #     #       # #
 #     # #     # #
 #     #  #####  #######
*/
TEST_CASE("hsl conversions", "[hsl]") {
  auto const color =
      cppc::color::from_hsl(Expected_hsla.h, Expected_hsla.s, Expected_hsla.l);

  CPPC_RGBA_TEST_SECTION()
  CPPC_RGBA255_TEST_SECTION()
  CPPC_RGBHEX_TEST_SECTION()
  CPPC_RGBAHEX_TEST_SECTION()
  CPPC_HSVA_TEST_SECTION()
  CPPC_CMYKA_TEST_SECTION()
}

/*
  #####  #     # #     # #    #
 #     # ##   ##  #   #  #   #
 #       # # # #   # #   #  #
 #       #  #  #    #    ###
 #       #     #    #    #  #
 #     # #     #    #    #   #
  #####  #     #    #    #    #
*/
TEST_CASE("cmyk conversions", "[cmyk]") {
  auto const color = cppc::color::from_cmyk(Expected_cmyka.c, Expected_cmyka.m,
                                            Expected_cmyka.y, Expected_cmyka.k);

  CPPC_RGBA_TEST_SECTION()
  CPPC_RGBA255_TEST_SECTION()
  CPPC_RGBHEX_TEST_SECTION()
  CPPC_RGBAHEX_TEST_SECTION()
  CPPC_HSVA_TEST_SECTION()
  CPPC_HSLA_TEST_SECTION()
}
