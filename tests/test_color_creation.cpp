#include <catch.hpp>

#include <cppc/cppc.hpp>

#include <iostream>

TEST_CASE("colors are correctly created", "[colors]") {

  SECTION("rgb creation") {
    auto rgb = cppc::color::from_rgb(0.3, 0.5, 0.7).rgba();
    std::cout << rgb << std::endl;
    REQUIRE(rgb.r == Approx(0.3));
    REQUIRE(rgb.g == Approx(0.5));
    REQUIRE(rgb.b == Approx(0.7));
    REQUIRE(rgb.a == Approx(1.0));
  }

  SECTION("rgba creation") {
    auto rgba = cppc::color::from_rgba(0.3, 0.5, 0.7, 0.5).rgba();
    std::cout << rgba << std::endl;
    REQUIRE(rgba.r == Approx(0.3));
    REQUIRE(rgba.g == Approx(0.5));
    REQUIRE(rgba.b == Approx(0.7));
    REQUIRE(rgba.a == Approx(0.5));
  }

  SECTION("rgb255 creation") {
    auto rgb = cppc::color::from_rgb255(77, 127, 178).rgba255();
    std::cout << rgb << std::endl;
    REQUIRE(+rgb.r == 77);
    REQUIRE(+rgb.g == 127);
    REQUIRE(+rgb.b == 178);
    REQUIRE(+rgb.a == 255);
  }

  SECTION("rgba255 creation") {
    auto rgba = cppc::color::from_rgba255(77, 127, 178, 127).rgba255();
    std::cout << rgba << std::endl;
    REQUIRE(+rgba.r == 77);
    REQUIRE(+rgba.g == 127);
    REQUIRE(+rgba.b == 178);
    REQUIRE(+rgba.a == 127);
  }

  SECTION("rgbHex creation") {
    auto rgb = cppc::color::from_rgbHex(0x4d7fb2).rgbHex();
    std::cout << rgb << std::endl;
    REQUIRE(rgb == 0x4d7fb2);
  }

  SECTION("rgbaHex creation") {
    auto rgba = cppc::color::from_rgbaHex(0x4d7fb27f).rgbaHex();
    std::cout << rgba << std::endl;
    REQUIRE(rgba == 0x4d7fb27f);
  }

  SECTION("hsv creation") {
    auto hsv = cppc::color::from_hsv(183.0, 0.5, 0.89).hsva();
    std::cout << hsv << std::endl;
    REQUIRE(hsv.h == Approx(183.0));
    REQUIRE(hsv.s == Approx(0.5));
    REQUIRE(hsv.v == Approx(0.89));
    REQUIRE(hsv.a == Approx(1.0));
  }

  SECTION("hsva creation") {
    auto hsva = cppc::color::from_hsva(183.0, 0.5, 0.89, 0.5).hsva();
    std::cout << hsva << std::endl;
    REQUIRE(hsva.h == Approx(183.0));
    REQUIRE(hsva.s == Approx(0.5));
    REQUIRE(hsva.v == Approx(0.89));
    REQUIRE(hsva.a == Approx(0.5));
  }

  SECTION("hsl creation") {
    auto hsla = cppc::color::from_hsl(183.0, 0.67, 0.66).hsla();
    std::cout << hsla << std::endl;
    REQUIRE(hsla.h == Approx(183));
    REQUIRE(hsla.s == Approx(0.67));
    REQUIRE(hsla.l == Approx(0.66));
    REQUIRE(hsla.a == Approx(1.0));
  }

  SECTION("hsla creation") {
    auto hsla = cppc::color::from_hsla(183.0, 0.67, 0.66, 0.5).hsla();
    std::cout << hsla << std::endl;
    REQUIRE(hsla.h == Approx(183));
    REQUIRE(hsla.s == Approx(0.67));
    REQUIRE(hsla.l == Approx(0.66));
    REQUIRE(hsla.a == Approx(0.5));
  }

  SECTION("cmyk creation") {
    auto cmyk = cppc::color::from_cmyk(0.5674, 0.2865, 0, 0.302).cmyka();
    std::cout << cmyk << std::endl;
    REQUIRE(cmyk.c == Approx(0.5674));
    REQUIRE(cmyk.m == Approx(0.2865));
    REQUIRE(cmyk.y == Approx(0));
    REQUIRE(cmyk.k == Approx(0.302));
    REQUIRE(cmyk.a == Approx(1.0));
  }

  SECTION("cmyka creation") {
    auto cmyka = cppc::color::from_cmyka(0.5674, 0.2865, 0, 0.302, 0.5).cmyka();
    std::cout << cmyka << std::endl;
    REQUIRE(cmyka.c == Approx(0.5674));
    REQUIRE(cmyka.m == Approx(0.2865));
    REQUIRE(cmyka.y == Approx(0));
    REQUIRE(cmyka.k == Approx(0.302));
    REQUIRE(cmyka.a == Approx(0.5));
  }
}
