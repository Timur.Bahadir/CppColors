#pragma once

#include "models/cmyk.hpp"
#include "models/hsl.hpp"
#include "models/hsv.hpp"
#include "models/rgb.hpp"
