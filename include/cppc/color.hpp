#pragma once

#include "models.hpp"

#include <cassert>
#include <cstdint>
#include <variant>

namespace cppc {

/**
 * @brief The core of the library. Used to create, convert and cache colors.
 *
 * A new color is created by using one of the from_* factory methods.
 * Each color uses a variant to cache the result of the last conversion.
 * The cached value is changed automatically when one of the read functions
 * is used.
 * The precision of a color can be defined using the template type in
 * combination wit the basic_color class. Two typedefs are provided with color
 * using floats and dcolor using doubles.
 *
 * @tparam T Type used for color values.
 */
template <typename T> class basic_color {
public:
  /**
   * @brief Static factory to create a new color from rgb values.
   *
   * The a component is set to 1.0.
   *
   * @param r Value used for r component of the color. Needs to be between 0.0
   * and 1.0.
   * @param g Value used for r component of the color. Needs to be between 0.0
   * and 1.0.
   * @param b Value used for r component of the color. Needs to be between 0.0
   * and 1.0.
   * @return basic_color<T> The created color with a cached rgba representation.
   */
  static basic_color<T> from_rgb(T const r, T const g, T const b) {
    return from_rgba(r, g, b, 1.0);
  }

  static basic_color<T> from_rgba(T const r, T const g, T const b, T const a) {
    assert(r <= 1.0 && g <= 1.0 && b <= 1.0 && a <= 1.0);
    assert(r >= 0.0 && g >= 0.0 && b >= 0.0 && a >= 0.0);
    return basic_color<T>{basic_rgba_values<T>{r, g, b, a}};
  }

  static basic_color<T> from_rgb255(uint8_t const r, uint8_t const g,
                                    uint8_t const b) {
    return from_rgba255(r, g, b, 255);
  }

  static basic_color<T> from_rgba255(uint8_t const r, uint8_t const g,
                                     uint8_t const b, uint8_t const a) {
    auto const nr = static_cast<T>(r / 255.0);
    auto const ng = static_cast<T>(g / 255.0);
    auto const nb = static_cast<T>(b / 255.0);
    auto const na = static_cast<T>(a / 255.0);
    return basic_color<T>{basic_rgba_values<T>{nr, ng, nb, na}};
  }

  static basic_color<T> from_rgbHex(uint32_t const hex) {
    assert(hex >= 0 && hex <= 0xffffff);

    uint32_t const nhex = (hex << 8) | 0xff;
    return from_rgbaHex(nhex);
  }

  static basic_color<T> from_rgbaHex(uint32_t hex) {
    uint8_t rgb[4];
    rgb[0] = static_cast<uint8_t>((hex >> 24) & 0xff);
    rgb[1] = static_cast<uint8_t>((hex >> 16) & 0xff);
    rgb[2] = static_cast<uint8_t>((hex >> 8) & 0Xff);
    rgb[3] = static_cast<uint8_t>((hex & 0Xff));
    return from_rgba255(rgb[0], rgb[1], rgb[2], rgb[3]);
  }

  static basic_color<T> from_hsv(T const h, T const s, T const v) {
    return from_hsva(h, s, v, 1.0);
  }

  static basic_color<T> from_hsva(T const h, T const s, T const v, T const a) {
    assert(h <= 360.0 && s <= 1.0 && v <= 1.0 && a <= 1.0);
    assert(h >= 0.0 && s >= 0.0 && v >= 0.0 && a >= 0.0);
    return basic_color<T>{basic_hsva_values<T>{h, s, v, a}};
  }

  static basic_color<T> from_hsl(T const h, T const s, T const l) {
    return from_hsla(h, s, l, 1.0);
  }

  static basic_color<T> from_hsla(T const h, T const s, T const l, T const a) {
    assert(h <= 360.0 && s <= 1.0 && l <= 1.0 && a <= 1.0);
    assert(h >= 0.0 && s >= 0.0 && l >= 0.0 && a >= 0.0);
    return basic_color<T>{basic_hsla_values<T>{h, s, l, a}};
  }

  static basic_color<T> from_cmyk(T const c, T const m, T const y, T const k) {
    assert(c <= 1.0 && m <= 1.0 && y <= 1.0 && k <= 1.0);
    assert(c >= 0.0 && m >= 0.0 && y >= 0.0 && k >= 0.0);
    return from_cmyka(c, m, y, k, 1.0);
  }

  static basic_color<T> from_cmyka(T const c, T const m, T const y, T const k,
                                   T const a) {
    assert(c <= 1.0 && m <= 1.0 && y <= 1.0 && k <= 1.0 && a <= 1.0);
    assert(c >= 0.0 && m >= 0.0 && y >= 0.0 && k >= 0.0 && a >= 0.0);
    return basic_color<T>{basic_cmyka_values<T>{c, m, y, k, a}};
  }

  basic_rgba_values<T> rgba() const {
    if (representation.index() != 0) {
      to_rgba();
    }
    return std::get<basic_rgba_values<T>>(representation);
  }

  rgba255_values rgba255() const {
    if (representation.index() != 0) {
      to_rgba();
    }

    return std::get<basic_rgba_values<T>>(representation).as_rgba255();
  }

  uint32_t rgbHex() const {
    if (representation.index() != 0) {
      to_rgba();
    }

    auto const rgb255 =
        std::get<basic_rgba_values<T>>(representation).as_rgba255();
    uint32_t const hex = (rgb255.r << 16) + (rgb255.g << 8) + (rgb255.b);
    return hex;
  }

  uint32_t rgbaHex() const {
    if (representation.index() != 0) {
      to_rgba();
    }

    auto const rgb255 =
        std::get<basic_rgba_values<T>>(representation).as_rgba255();
    uint32_t const hex =
        (rgb255.r << 24) + (rgb255.g << 16) + (rgb255.b << 8) + (rgb255.a);
    return hex;
  }

  basic_hsva_values<T> hsva() const {
    if (representation.index() != 1) {
      to_hsva();
    }
    return std::get<basic_hsva_values<T>>(representation);
  }

  basic_hsla_values<T> hsla() const {
    if (representation.index() != 2) {
      to_hsla();
    }
    return std::get<basic_hsla_values<T>>(representation);
  }

  basic_cmyka_values<T> cmyka() const {
    if (representation.index() != 3) {
      to_cmyka();
    }
    return std::get<basic_cmyka_values<T>>(representation);
  }

private:
  using int_rep = std::variant<basic_rgba_values<T>, basic_hsva_values<T>,
                               basic_hsla_values<T>, basic_cmyka_values<T>>;

  basic_color(int_rep value) : representation{value} {}

  mutable int_rep representation;

  void to_rgba() const {
    switch (representation.index()) {
    case 1:
      representation = static_cast<basic_rgba_values<T>>(
          std::get<basic_hsva_values<T>>(representation));
      break;
    case 2:
      representation = static_cast<basic_rgba_values<T>>(
          std::get<basic_hsla_values<T>>(representation));
      break;
    case 3:
      representation = static_cast<basic_rgba_values<T>>(
          std::get<basic_cmyka_values<T>>(representation));
      break;
    default:
      assert("Tried to cast from rgb to rgb.");
      break;
    }
  }

  void to_hsva() const {
    switch (representation.index()) {
    case 0:
      representation = static_cast<basic_hsva_values<T>>(
          std::get<basic_rgba_values<T>>(representation));
      break;
    case 2:
      representation = static_cast<basic_hsva_values<T>>(
          std::get<basic_hsla_values<T>>(representation));
      break;
    case 3:
      representation = static_cast<basic_hsva_values<T>>(
          std::get<basic_cmyka_values<T>>(representation));
      break;
    default:
      assert("Tried to cast from hsva to hsva.");
      break;
    }
  }

  void to_hsla() const {
    switch (representation.index()) {
    case 0:
      representation = static_cast<basic_hsla_values<T>>(
          std::get<basic_rgba_values<T>>(representation));
      break;
    case 1:
      representation = static_cast<basic_hsla_values<T>>(
          std::get<basic_hsva_values<T>>(representation));
      break;
    case 3:
      representation = static_cast<basic_hsla_values<T>>(
          std::get<basic_cmyka_values<T>>(representation));
      break;
    default:
      assert("Tried to cast from hsla to hsla.");
      break;
    }
  }

  void to_cmyka() const {
    switch (representation.index()) {
    case 0:
      representation = static_cast<basic_cmyka_values<T>>(
          std::get<basic_rgba_values<T>>(representation));
      break;
    case 1:
      representation = static_cast<basic_cmyka_values<T>>(
          std::get<basic_hsva_values<T>>(representation));
      break;
    case 2:
      representation = static_cast<basic_cmyka_values<T>>(
          std::get<basic_hsla_values<T>>(representation));
      break;
    default:
      assert("Tried to cast from cmyka to cmyka.");
      break;
    }
  }
};

using color = basic_color<float>;
using dcolor = basic_color<double>;

} // namespace cppc
