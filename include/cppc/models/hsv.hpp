#pragma once

#include "fwd/cmyk_fwd.hpp"
#include "fwd/hsl_fwd.hpp"
#include "fwd/hsv_fwd.hpp"
#include "fwd/rgb_fwd.hpp"

#include <cassert>
#include <cstdint>
#include <iomanip>
#include <ostream>

namespace cppc {

/**
 * @brief A POD-struct to hold hsva information.
 *
 * @tparam T The type used for h, s, v and a values.
 */
template <typename T> struct basic_hsva_values {
  T h{0}, s{0}, v{0}, a{0};

  friend std::ostream &operator<<(std::ostream &os,
                                  basic_hsva_values const &m) {
    return os << "[HSVA: " << std::setprecision(4) << m.h << ", " << m.s << ", "
              << m.v << ", " << m.a << "]";
  }

  operator basic_rgba_values<T>() const {
    assert(h <= 360.0 && s <= 1.0 && v <= 1.0 && a <= 1.0);
    assert(h >= 0.0 && s >= 0.0 && v >= 0.0 && a >= 0.0);

    uint8_t const hi = std::floor(h / 60.0);
    T const nf = (h / 60.0) - hi;
    T const np = v * (1.0 - s);
    T const nq = v * (1.0 - s * nf);
    T const nt = v * (1.0 - s * (1.0 - nf));

    assert(np <= 360.0 && nq <= 1.0 && nt <= 1.0);
    assert(np >= 0.0 && nq >= 0.0 && nt >= 0.0);

    if (hi == 0 || hi == 6) {
      return basic_rgba_values<T>{v, nt, np, a};
    }

    switch (hi) {
    case 1:
      return basic_rgba_values<T>{nq, v, np, a};
    case 2:
      return basic_rgba_values<T>{np, v, nt, a};
    case 3:
      return basic_rgba_values<T>{np, nq, v, a};
    case 4:
      return basic_rgba_values<T>{nt, np, v, a};
    case 5:
      return basic_rgba_values<T>{v, np, nq, a};
    default:
      assert("Value of hi was outside of bounds, that should be impossible.");
      return basic_rgba_values<T>{-1.0, -1.0, -1.0, -1.0};
    }
  }

  operator basic_hsla_values<T>() const {
    assert(h <= 360.0 && s <= 1.0 && v <= 1.0 && a <= 1.0);
    assert(h >= 0.0 && s >= 0.0 && v >= 0.0 && a >= 0.0);

    T const l1 = (2.0 - s) * v;
    T const ns = ((s * v) / (l1 <= 1.0 ? l1 : 2.0 - l1));
    T const nl = l1 * 0.5;

    assert(ns <= 1.0 && nl <= 1.0);
    assert(ns >= 0.0 && nl >= 0.0);

    return basic_hsla_values<T>{h, ns, nl, a};
  }

  operator basic_cmyka_values<T>() const {
    auto const rgba = static_cast<basic_rgba_values<T>>(*this);
    auto const cmyka = static_cast<basic_cmyka_values<T>>(rgba);
    return cmyka;
  }
};

} // namespace cppc
