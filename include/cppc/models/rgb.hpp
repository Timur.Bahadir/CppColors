#pragma once

#include "fwd/cmyk_fwd.hpp"
#include "fwd/hsl_fwd.hpp"
#include "fwd/hsv_fwd.hpp"
#include "fwd/rgb_fwd.hpp"

#include <cassert>
#include <cstdint>
#include <iomanip>
#include <ostream>

#ifndef ZERO_THRESHOLD
#define ZERO_THRESHOLD 0.01f
#endif // !ZERO_THRESHOLD

namespace cppc {

namespace {

template <typename T>
T calc_h(T const min, T const max, T const delta,
         basic_rgba_values<T> const &rgba) {
  T h{0};
  if (delta < ZERO_THRESHOLD) {
    h = 0;
  } else if (max == rgba.r) {
    h = 60.0 * ((rgba.g - rgba.b) / delta);
  } else if (max == rgba.g) {
    h = 60.0 * (2.0 + (rgba.b - rgba.r) / delta);
  } else if (max == rgba.b) {
    h = 60.0 * (4.0 + (rgba.r - rgba.g) / delta);
  }
  h += h < 0.0 ? 360.0 : 0.0;
  return h;
}

} // namespace

/**
 * @brief A POD-struct to hold rgba information.
 *
 * @tparam T The type used for r, g, b and a values.
 */
template <typename T> struct basic_rgba_values {
  T r{0}, g{0}, b{0}, a{0};

  friend std::ostream &operator<<(std::ostream &os,
                                  basic_rgba_values const &m) {
    // the pluses are required to print the numerical value instead of being
    // interpreted as ascii characters for rgba255 (uint8_t).
    return os << "[RGBA: " << std::setprecision(4) << +m.r << ", " << +m.g
              << ", " << +m.b << ", " << +m.a << "]";
  }

  rgba255_values as_rgba255() const {
    assert(r <= 1.0 && g <= 1.0 && b <= 1.0 && a <= 1.0);
    assert(r >= 0.0 && g >= 0.0 && b >= 0.0 && a >= 0.0);

    auto const nr = static_cast<uint8_t>(std::round(r * 255.0));
    auto const ng = static_cast<uint8_t>(std::round(g * 255.0));
    auto const nb = static_cast<uint8_t>(std::round(b * 255.0));
    auto const na = static_cast<uint8_t>(std::round(a * 255.0));

    assert(nr <= 255 && ng <= 255 && nb <= 255 && na <= 255);
    assert(nr >= 0 && ng >= 0 && nb >= 0 && na >= 0);

    return rgba255_values{nr, ng, nb, na};
  }

  operator basic_hsva_values<T>() const {
    assert(r <= 1.0 && g <= 1.0 && b <= 1.0 && a <= 1.0);
    assert(r >= 0.0 && g >= 0.0 && b >= 0.0 && a >= 0.0);

    T const min = std::min({r, g, b});
    T const max = std::max({r, g, b});
    T const delta = max - min;

    basic_hsva_values<T> hsva{0, 0, 0, a};

    hsva.h = calc_h(min, max, delta, *this);

    if (std::abs(max) < ZERO_THRESHOLD) {
      hsva.s = 0;
    } else {
      hsva.s = delta / max;
    }

    hsva.v = max;

    assert(hsva.h <= 360.0 && hsva.s <= 1.0 && hsva.v <= 1.0 && hsva.a <= 1.0);
    assert(hsva.h >= 0.0 && hsva.s >= 0.0 && hsva.v >= 0.0 && hsva.a >= 0.0);

    return hsva;
  }

  operator basic_hsla_values<T>() const {
    assert(r <= 1.0 && g <= 1.0 && b <= 1.0 && a <= 1.0);
    assert(r >= 0.0 && g >= 0.0 && b >= 0.0 && a >= 0.0);

    T const min = std::min({r, g, b});
    T const max = std::max({r, g, b});
    T const delta = max - min;

    basic_hsla_values<T> hsla{0, 0, 0, a};

    hsla.h = calc_h(min, max, delta, *this);

    if (std::abs(max) < ZERO_THRESHOLD || 1.0 - min < ZERO_THRESHOLD) {
      hsla.s = 0;
    } else {
      hsla.s = delta / (1.0 - (std::abs(max + min) - 1.0));
    }

    hsla.l = (max + min) / 2.0;

    assert(hsla.h <= 360.0 && hsla.s <= 1.0 && hsla.l <= 1.0 && hsla.a <= 1.0);
    assert(hsla.h >= 0.0 && hsla.s >= 0.0 && hsla.l >= 0.0 && hsla.a >= 0.0);

    return hsla;
  }

  operator basic_cmyka_values<T>() const {
    assert(r <= 1.0 && g <= 1.0 && b <= 1.0 && a <= 1.0);
    assert(r >= 0.0 && g >= 0.0 && b >= 0.0 && a >= 0.0);

    T const k = 1.0 - std::max({r, g, b});
    T const c = (1.0 - r - k) / (1.0 - k);
    T const m = (1.0 - g - k) / (1.0 - k);
    T const y = (1.0 - b - k) / (1.0 - k);

    assert(c <= 1.0 && m <= 1.0 && y <= 1.0 && k <= 1.0);
    assert(c >= 0.0 && m >= 0.0 && y >= 0.0 && k >= 0.0);

    return basic_cmyka_values<T>{c, m, y, k, a};
  }
};

} // namespace cppc
