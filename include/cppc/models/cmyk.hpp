#pragma once

#include "fwd/cmyk_fwd.hpp"
#include "fwd/hsl_fwd.hpp"
#include "fwd/hsv_fwd.hpp"
#include "fwd/rgb_fwd.hpp"

#include <cassert>
#include <cstdint>
#include <iomanip>
#include <ostream>

namespace cppc {
/**
 * @brief A POD-struct to hold cmyka information.
 *
 * @tparam T The type used for c, m, y, k and a values.
 */
template <typename T> struct basic_cmyka_values {
  T c{0}, m{0}, y{0}, k{0}, a{0};

  friend std::ostream &operator<<(std::ostream &os,
                                  basic_cmyka_values const &m) {
    return os << "[CMYKA: " << std::setprecision(4) << m.c << ", " << m.m
              << ", " << m.y << ", " << m.k << ", " << m.a << "]";
  }

  operator basic_rgba_values<T>() const {
    assert(c <= 1.0 && m <= 1.0 && y <= 1.0 && k <= 1.0 && a <= 1.0);
    assert(c >= 0.0 && m >= 0.0 && y >= 0.0 && k >= 0.0 && a >= 0.0);

    T const r = (1.0 - c) * (1.0 - k);
    T const g = (1.0 - m) * (1.0 - k);
    T const b = (1.0 - y) * (1.0 - k);

    assert(r <= 1.0 && g <= 1.0 && b <= 1.0 && a <= 1.0);
    assert(r >= 0.0 && g >= 0.0 && b >= 0.0 && a >= 0.0);

    return basic_rgba_values<T>{r, g, b, a};
  }

  operator basic_hsva_values<T>() const {
    auto const rgba = static_cast<basic_rgba_values<T>>(*this);
    auto const hsva = static_cast<basic_hsva_values<T>>(rgba);
    return hsva;
  }

  operator basic_hsla_values<T>() const {
    auto const rgba = static_cast<basic_rgba_values<T>>(*this);
    auto const hsla = static_cast<basic_hsla_values<T>>(rgba);
    return hsla;
  }
};

} // namespace cppc
