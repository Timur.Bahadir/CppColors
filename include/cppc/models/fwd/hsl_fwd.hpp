#pragma once

namespace cppc {

template <typename T> struct basic_hsla_values;

using hsla_values = basic_hsla_values<float>;

using d_hsla_values = basic_hsla_values<double>;

} // namespace cppc
