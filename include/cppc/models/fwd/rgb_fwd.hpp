#pragma once

#include <cstdint>

namespace cppc {

template <typename T> struct basic_rgba_values;

using rgba_values = basic_rgba_values<float>; /**< Default typedef for
                                                 basic_rgba_values using float
                                                 as type */

using rgba_d_values = basic_rgba_values<double>; /**< typedef for
                                                 basic_rgba_values using double
                                                 as type */

using rgba255_values = basic_rgba_values<uint8_t>; /**< typedef used for
                                                    basic_rgba_values values
                                                    using uint8_t as type */

} // namespace cppc
