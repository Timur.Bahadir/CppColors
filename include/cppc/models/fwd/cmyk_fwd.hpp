#pragma once

namespace cppc {

template <typename T> struct basic_cmyka_values;

using cmyka_values = basic_cmyka_values<float>;

using d_cmyka_values = basic_cmyka_values<double>;

} // namespace cppc
