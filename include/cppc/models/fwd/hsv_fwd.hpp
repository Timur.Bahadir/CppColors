#pragma once

namespace cppc {

template <typename T> struct basic_hsva_values;

using hsva_values = basic_hsva_values<float>;
using d_hsva_values = basic_hsva_values<double>;

} // namespace cppc
