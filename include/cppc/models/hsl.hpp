#pragma once

#include "fwd/cmyk_fwd.hpp"
#include "fwd/hsl_fwd.hpp"
#include "fwd/hsv_fwd.hpp"
#include "fwd/rgb_fwd.hpp"

#include <cassert>
#include <cstdint>
#include <iomanip>
#include <ostream>

namespace cppc {

/**
 * @brief A POD-struct to hold hsla information.
 *
 * @tparam T The type used for h, s, l and a values.
 */
template <typename T> struct basic_hsla_values {
  T h{0}, s{0}, l{0}, a{0};

  friend std::ostream &operator<<(std::ostream &os,
                                  basic_hsla_values const &m) {
    return os << "[HSLA: " << std::setprecision(4) << m.h << ", " << m.s << ", "
              << m.l << ", " << m.a << "]";
  }

  operator basic_rgba_values<T>() const {
    auto const hsva = static_cast<basic_hsva_values<T>>(*this);
    auto const rgba = static_cast<basic_rgba_values<T>>(hsva);
    return rgba;
  }

  operator basic_hsva_values<T>() const {
    assert(h <= 360.0 && s <= 1.0 && l <= 1.0 && a <= 1.0);
    assert(h >= 0.0 && s >= 0.0 && l >= 0.0 && a >= 0.0);

    T const l1 = l * 2.0;
    T const s1 = s * ((l1 <= 1) ? l1 : 2.0 - l1);
    T const ns = (2.0 * s1) / (l1 + s1);
    T const nv = (l1 + s1) / 2.0;

    assert(ns <= 1.0 && nv <= 1.0);
    assert(ns >= 0.0 && nv >= 0.0);

    return basic_hsva_values<T>{h, ns, nv, a};
  }

  operator basic_cmyka_values<T>() const {
    auto const rgba = static_cast<basic_rgba_values<T>>(*this);
    auto const cmyka = static_cast<basic_cmyka_values<T>>(rgba);
    return cmyka;
  }
};

} // namespace cppc
