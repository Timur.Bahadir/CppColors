# CppColors

CppColors is a library to handle and convert colors in C++.

## Usage

``` cpp
#include "cppc/cppc.hpp"

...

auto myColor = cppc::color::from_rgb255(113, 221, 227);

auto rgb255 = myColor.rgba255();
std::cout << rgb << std::endl; // prints: "[RGBA: 113, 221, 227, 255]"

auto rgbHex = myColor.rgbHex();
std::cout << hex << rgbHex << std::endl; // prints: "4d7fb2"

auto rgb = myColor.rgba();
std::cout << rgb << std::endl; // prints: "[RGBA: 0.443, 0.867, 0.89, 1]"

auto hsv = myColor.hsva();
std::cout << hsv << std::endl; // prints: "[HSVA: 183, 0.5, 0.89, 1]"

auto hsl = myColor.hsla();
std::cout << hsl << std::endl; // prints: "[HSLA: 183, 0.671, 0.667, 1]"

auto cmyk = myColor.cmyka();
std::cout << cmyk << std::endl; // prints: "[CMYKA: 0.5022, 0.0264, 0, 0.1098, 1]"
```

## Conversion and Testing

The current state of implementation and testing of color formats and conversions
can be seen here.

| from \\ to | rgb         | rgb255       | rgbHex       | hsv         | hsl         | cmyk         |
| :--------: | :---------: | :----------: | :----------: | :---------: | :---------: | :----------: |
| rgb        |        --   | `#0F0` Impl  | `#0F0` Impl  | `#F70` Impl | `#F70` Impl | `#0F0` Impl  |
| rgb255     | `#0F0` Impl |        --    | `#0F0` Impl  | `#00F` Impl | `#00F` Impl | `#00F` Impl  |
| rgbHex     | `#0F0` Impl | `#0F0` Impl  |        --    | `#00F` Impl | `#00F` Impl | `#00F` Impl  |
| hsv        | `#F70` Impl | `#00F` Impl  | `#00F` Impl  |        --   | `#0F0` Impl | `#00F` Ind   |
| hsl        | `#F70` Ind  | `#00F` Impl  | `#00F` Impl  | `#0F0` Impl |        --   | `#00F` Ind   |
| cmyk       | `#0F0` Impl | `#00F` Impl  | `#00F` Impl  | `#00F` Ind  | `#00F` Ind  |        --    |

| Text | Meaning                |
| ---- | ---------------------- |
| --   | Not required           |
| Impl | Fully implemented      |
| Ind  | Indirectly implemented |
| Plan | Planned                |

| Color  | Meaning                                                         |
| ------ | --------------------------------------------------------------- |
| Red    | Untested                                                        |
| Orange | Tested                                                          |
| Green  | Statement coverage                                              |
| Blue   | No direct testing needed (conversion happens over other format) |

## Planned

These features and changes are currently planned.

- Operators
  - Comparison
  - Multiplication / Addition
- Full test coverage
- Proper documentation
- CI
